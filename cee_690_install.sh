#!/bin/bash
sudo apt-get update
sudo apt-get -y dist-upgrade 
sudo apt-get autoremove -y
apt-get install -y --no-install-recommends \
    software-properties-common \
    wget \
    pwgen \
    net-tools \
    build-essential \
    git \
    bzip2 \
    vim \
    unzip 


sudo apt-get -y install \
   libcairo2-dev \
   libjpeg-dev  \
   libgif-dev \
   unzip \
   build-essential \
   libpython2.7-dev \
   g++ \
   python-dev \
   autotools-dev \
   libicu-dev \
   libbz2-dev \
   cmake \
   libxml2-dev \
   libxslt1-dev \
   libsqlite3-dev \
   python-pip \
   libeigen3-dev \
   subversion \
   libssl-dev \
   libcurl4-openssl-dev \
   nfs-common

sudo pip install \
   numpy \
   cython \
   lxml \
   nose \
   coverage \
   zope.interface \
   py-bcrypt \
   sqlalchemy

sudo pip install setuptools --upgrade

