#!/bin/bash

set -x

./cee_690_install.sh | tee /tmp/rapid_image_status.txt

if [[ ${PIPESTATUS[0]} == 0 ]]; then
  echo "Installer Succeeded"
  echo "success" > /tmp/rapid_image_complete
else
  echo "Installer failed"
  echo "fail" > /tmp/rapid_image_complete
fi
